package ru.t1.semikolenov.tm.api.repository.model;

import ru.t1.semikolenov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
