package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.service.dto.IProjectDtoService;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.semikolenov.tm.api.service.dto.ITaskDtoService;
import ru.t1.semikolenov.tm.api.service.dto.IUserDtoServiceO;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    IUserDtoServiceO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}